# Jenkins docker image

## Usage

```
git clone https://gitlab.com/charon_deployer_test/jenkins.git
cd jenkins/
docker build . -t jenkins_devel
docker volume create jenkins_home
docker run -d -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home jenkins_devel:latest
```